﻿    
###########################################################################
   ____ ____ ____ ____ ____ ____ _________ ____ ____ ____ ____ ____ ____ 
  ||S |||h |||a |||r |||e |||d |||       |||M |||e |||m |||o |||r |||y ||
  ||__|||__|||__|||__|||__|||__|||_______|||__|||__|||__|||__|||__|||__||
  |/__\|/__\|/__\|/__\|/__\|/__\|/_______\|/__\|/__\|/__\|/__\|/__\|/__\|



####################### Compiler Installation ########################
The use of the gcc compiler is suggested, you can download it in the 
following link.

https://sourceforge.net/projects/tdm-gcc/

When installing TDM-GCC we can install the support for 64bits, 32bits 
or both. 

By default the installation path are:
C:\TDM-GCC32
C:\TDM-GCC-64

After installing the gcc compiler we install the CodeLite IDE.
If we already have the CodeLite IDE installed, we must add the new compilers

Add new compilers: 
					-We open CodeLite	
					- Clic "Settings"
					- Clic "Build Settings"
					- In the upper left, click on the icon of a folder with 
					  a magnifying "Scan for compilers in this computer"
					- You must find the new compilers under the MinGW tag:
					  MinGW (TDM-GCC-64) C:\TDM-GCC-64
					  MinGW (TDM-GCC-32) C:\TDM-GCC-32
					- Clic OK
					

#################### Choose compiler - x86 and x4 ###############

Once the IDE recognizes the location of our compiler, we can choose 
which one we want to compile with:
	- Clic derecho en nuestro proyecto (smClient or smClientJava)
	- Clic "settings"
	- Clic "General"
	- Compiler:  "MinGW (TDM-GCC-64)" "MinGW (TDM-GCC-64)"
	- Apply and Accept

Then you can clean, build or rebuild.


Thanks for use smClient (The client for named shared memories administration)
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'               `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$$$$$$$$$$$$$$$$$$$$$$$$$'                   `$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$'`$$$$$$$$$$$$$'`$$$$$$!                       !$$$$$$'`$$$$$$$$$$$$$'`$$$
$$$$  $$$$$$$$$$$  $$$$$$$                         $$$$$$$  $$$$$$$$$$$  $$$$
$$$$. `$' \' \$`  $$$$$$$!                         !$$$$$$$  '$/ `/ `$' .$$$$
$$$$$. !\  i  i .$$$$$$$$                           $$$$$$$$. i  i  /! .$$$$$
$$$$$$   `--`--.$$$$$$$$$                           $$$$$$$$$.--'--'   $$$$$$
$$$$$$L        `$$$$$^^$$                           $$^^$$$$$'        J$$$$$$
$$$$$$$.   .'   ""~   $$$    $.                 .$  $$$   ~""   `.   .$$$$$$$
$$$$$$$$.  ;      .e$$$$$!    $$.             .$$  !$$$$$e,      ;  .$$$$$$$$
$$$$$$$$$   `.$$$$$$$$$$$$     $$$.         .$$$   $$$$$$$$$$$$.'   $$$$$$$$$
$$$$$$$$    .$$$$$$$$$$$$$!     $$`$$$$$$$$'$$    !$$$$$$$$$$$$$.    $$$$$$$$
$JT&yd$     $$$$$$$$$$$$$$$$.    $    $$    $   .$$$$$$$$$$$$$$$$     $by&TL$
                                 $    $$    $
                                 $.   $$   .$
                                 `$        $'
                                  `$$$$$$$$'


