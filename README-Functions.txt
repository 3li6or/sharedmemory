﻿   
###########################################################################
   ____ ____ ____ ____ ____ ____ _________ ____ ____ ____ ____ ____ ____ 
  ||S |||h |||a |||r |||e |||d |||       |||M |||e |||m |||o |||r |||y ||
  ||__|||__|||__|||__|||__|||__|||_______|||__|||__|||__|||__|||__|||__||
  |/__\|/__\|/__\|/__\|/__\|/__\|/_______\|/__\|/__\|/__\|/__\|/__\|/__\|


############################ Descriptions ##################################
This named shared memory management project consists of three folders:	
	- Examples of using dynamic link library in several programming languages
	- Library of functions (Code of creation, management and access to 
	  named shared memories, you can also find all the projects with the 
	  codes of creation of shared link libraries.
	- Main program code for creating and managing shared memories (Sm Panel)
	
Examples.- Several examples in several programming languages of how to 
import and use the library and its functions are attached.
NOTE: The difference between working with floating or double variable type 
is the number of decimals, floating type variables support maximum 
between 4 to 5 decimal places unlike double variable variables that 
can work with many more decimal places.

Function library.- You will find three main folders corresponding to the 
folder that contains the libraries compiled for both 32 bits and 
64 bit architecture.
	- build-Debug\lib\AllGeneratedLibraries\
The project with the standard dynamic link library generation codes 
	- smClient\.
The project with the dynamic link library library generation code to be 
	used in Java Projects.
	- smClientJava\
 For this library, programming language C was used and CodeLite as IDE.

Memory creation panel.- Corresponds to the code of the application 
that creates and manages the named shared memories "Sm Panel". 
In this panel the user has the option to create one or several 
named shared memories by setting the name of the memories, the number of 
variables that you want to store in each memory and the type of variable 
that you are going to store. 
NOTE: The creation and administration of named shared memories can also 
be done through the dynamic link library by calling the named shared memory 
creation and releasing function in case the programmer wishes to manage 
the creation of memories.



############################ Funciones ####################################
The following are the functions found in the dynamic link library:

********************************************
FUNCTION: 
	int open_Memory(char *memoryName, int typeCode);
DESCIPTION: 
	this function opens a memory with a specific name (memoryName) 
	and a specified type (typeCode): 
PARAMETERS: 
	memoryName : We specify the name of the memory we wish to open
	typeCode   : We specify the code of the type of memory we 
				 want to open, it can be:
					1 = Integer
					2 = Float
					3 = Double
					4 = String (max 13 character), you can change 
						this limit in the implementation code.
RETURN:
	0 = If it runs correctly
	1 = if there was an error

*********************************************
FUNCTION: 
	int get_Int(char *memoryName, int valuePosition);
 DESCIPTION: 
	This function returns the value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
 PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to read the value
 RETURN:
	returns the integer type value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
	
*********************************************
FUNCTION: 
	void set_Int(char *memoryName, int valuePosition,int dValue);
DESCIPTION: 
	This function stores a value (dValue) in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to write the value
	dValue		  : The value that will be stored

*********************************************
FUNCTION: 
	void set_Float(char *memoryName, int valuePosition, float fValue);
DESCIPTION: 
	This function stores a value (fValue) in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to write the value
	fValue		  : The value that will be stored
	
*********************************************
FUNCTION: 
	float get_Float(char *memoryName, int valuePosition);
DESCIPTION: 
	This function returns the value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to read the value
RETURN:
	returns the float type value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
	

*********************************************
FUNCTION: 
	void set_Double(char *memoryName, int valuePosition, double lfValue);
DESCIPTION: 
	This function stores a value (lfValue) in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to write the value
	lfValue		  : The value that will be stored
	

*********************************************
FUNCTION: 
	double get_Double(char *memoryName, int valuePosition);
DESCIPTION: 
	This function returns the value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to read the value
RETURN:
	returns the double type value stored in position (valuePosition)
	belonging to memory with the name (memoryName)
	
	
*********************************************
FUNCTION: 
	void set_String(char *memoryName, int valuePosition, char *strValue);
DESCIPTION: 
	This function stores a value (strValue) in position (valuePosition)
	belonging to memory with the name (memoryName).
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with.
	valuePosition : We specify the position we want to write the value.
	strValue	  : The string value that will be stored.


*********************************************
FUNCTION: 
	void get_String(char *memoryName, int valuePosition, char *strRetValue);
DESCIPTION: 
	This function returns the string of characters stored
	in position (valuePosition)
	belonging to memory with the name (memoryName)
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to read the value
	strRetValue	  : This pointer will point to the character string stored 
					in the (valuePosition) position.


*********************************************
FUNCTION: 
	void get_String_Matlab(char *memoryName, int valuePosition, 
							char **strRetValue);
DESCIPTION: 
	This function returns the string of characters stored
	in position (valuePosition) belonging to memory with the name (memoryName) 
	This function is special for matlab, since matlab requires 
	a double pointer.
	
PARAMETERS: 
	memoryName    : We specify the name of the memory we wish to work with
	valuePosition : We specify the position we want to read the value
	strRetValue	  : This pointer will point to the character string stored 
					in the (valuePosition) position.
					
					
*********************************************
FUNCTION: 
	int create_Memory(char *memoryName, int quantity, int typeCode);
DESCIPTION: 
	This function creates a shared memory labeled with the name 
	specified in (memoryName), which may contain a quantity of values 
	specified in (quantity), and the values will be of the type 
	specified in (typeCode)
PARAMETERS: 
	memoryName : We specify the name of the memory we want to create
	quantity   : We specify the maximum amount of values that we can store 
				 in this memory
	typeCode   : We specify the code of the type of memory we 
				 want to create, it can be:
					1 = Integer
					2 = Float
					3 = Double
					4 = String (max 13 character), you can change 
						this limit in the implementation code.
RETURN:
	0 = If it runs correctly
	1 = if there was an error


***********************************************
FUNCTION: 
	void free_Memories();
DESCIPTION: 
	This function frees managed memory for the creation of named
	shared memories.	


**********************************************
FUNCTION: 
	void free_Views();
DESCIPTION: 
	This function frees managed memory for the creation of 
	views of named shared memories.



$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'               `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$$$$$$$$$$$$$$$$$$$$$$$$$'                   `$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$'`$$$$$$$$$$$$$'`$$$$$$!                       !$$$$$$'`$$$$$$$$$$$$$'`$$$
$$$$  $$$$$$$$$$$  $$$$$$$                         $$$$$$$  $$$$$$$$$$$  $$$$
$$$$. `$' \' \$`  $$$$$$$!                         !$$$$$$$  '$/ `/ `$' .$$$$
$$$$$. !\  i  i .$$$$$$$$                           $$$$$$$$. i  i  /! .$$$$$
$$$$$$   `--`--.$$$$$$$$$                           $$$$$$$$$.--'--'   $$$$$$
$$$$$$L        `$$$$$^^$$                           $$^^$$$$$'        J$$$$$$
$$$$$$$.   .'   ""~   $$$    $.                 .$  $$$   ~""   `.   .$$$$$$$
$$$$$$$$.  ;      .e$$$$$!    $$.             .$$  !$$$$$e,      ;  .$$$$$$$$
$$$$$$$$$   `.$$$$$$$$$$$$     $$$.         .$$$   $$$$$$$$$$$$.'   $$$$$$$$$
$$$$$$$$    .$$$$$$$$$$$$$!     $$`$$$$$$$$'$$    !$$$$$$$$$$$$$.    $$$$$$$$
$JT&yd$     $$$$$$$$$$$$$$$$.    $    $$    $   .$$$$$$$$$$$$$$$$     $by&TL$
                                 $    $$    $
                                 $.   $$   .$
                                 `$        $'
                                  `$$$$$$$$'

